#ifndef SPOTIFY_H
#define SPOTIFY_H

#include <QObject>
#include <QtCore>
#include "jcon/json_rpc_client.h"
#include "jcon/json_rpc_websocket_client.h"
#include <QNetworkAccessManager>
#include <QOAuth2AuthorizationCodeFlow>

class Spotify : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool connectionStatus READ getConnectionStatus NOTIFY connectionStatusChanged)
    Q_PROPERTY(QString artist MEMBER artist NOTIFY trackInfoChanged)
    Q_PROPERTY(QString trackName MEMBER trackName NOTIFY trackInfoChanged)
    Q_PROPERTY(int trackNumber MEMBER trackNumber NOTIFY trackInfoChanged)
    Q_PROPERTY(QString coverUrl MEMBER coverUrl NOTIFY coverChanged)
    Q_PROPERTY(int trackLength MEMBER trackLength NOTIFY trackInfoChanged)
    Q_PROPERTY(QString trackLengthLabel MEMBER trackLengthLabel NOTIFY trackInfoChanged)
    Q_PROPERTY(QString trackPositionLabel MEMBER trackPositionLabel NOTIFY trackPositionLabelChanged)
    Q_PROPERTY(int trackPosition MEMBER trackPosition NOTIFY trackPositionLabelChanged)
    Q_PROPERTY(bool playbackState MEMBER playbackState NOTIFY playbackStateChanged)
    Q_PROPERTY(bool changedWithSwipe MEMBER changedWithSwipe)
    Q_PROPERTY(bool changedWithTrackEnd MEMBER changedWithTrackEnd)
    Q_PROPERTY(bool UIready MEMBER UIready)
    Q_PROPERTY(int playlistSize MEMBER playlistSize NOTIFY playlistChanged)
    Q_PROPERTY(int volume MEMBER volume)
    Q_PROPERTY(QVector<QString> coverList MEMBER coverList NOTIFY playlistChanged)
    Q_PROPERTY(QVector<QString> tracklist MEMBER tracklist NOTIFY playlistChanged)
    Q_PROPERTY(QVector<QString> tracklistAlbums MEMBER tracklistAlbums NOTIFY playlistChanged)
    Q_PROPERTY(QVector<QString> tracklistArtists MEMBER tracklistArtists NOTIFY playlistChanged)
    Q_PROPERTY(QVector<int> tracklistIDs MEMBER tracklistIDs NOTIFY playlistChanged)
public:
    Spotify();
    bool getConnectionStatus();
    bool getPlaybackState();

public slots:
    void playClicked(int tlid);
    void resumeClicked();
    void pauseClicked();
    void nextClicked();
    void previousClicked();
    void getTrackPosition();
    void getTrackInfo();
    QString getTrackCover(QString url);
    void trackSeek(int position);
    void getTracklist();
    void getTrackNumber();
    void setVolume(int newVolume);

private Q_SLOTS:
    void parseEvent(const QJsonObject obj);
    void granted();
    void authStatusChanged(QAbstractOAuth::Status status);

Q_SIGNALS:
    void connectionStatusChanged(QString newStatus);
    void trackInfoChanged();
    void coverChanged();
    void trackPositionLabelChanged();
    void playbackStateChanged();
    void playlistChanged();
    void trackEnded();
    void uiReady();
    void uiNotReady();

private:
    jcon::JsonRpcClient* rpc_client;
    QString mopidyServerAddr;
    QOAuth2AuthorizationCodeFlow spotifyWebApi;
    QTimer *trackMslabelTimer;
    QString trackName;
    QString artist;
    QString artistUrl;
    QString album;
    QString albumUrl;
    QString coverUrl;
    int trackLength;
    QString trackLengthLabel;
    int trackPosition;
    QString trackPositionLabel;
    bool playbackState;
    bool changedWithSwipe;
    bool changedWithTrackEnd;
    int playlistSize;
    int trackNumber;
    int volume;
    QVector<QString> tracklist;
    QVector<int> tracklistIDs;
    QVector<QString> tracklistArtists;
    QVector<QString> tracklistAlbums;
    QVector<QString> coverList;
    bool UIready;

    void getVolume();
};

#endif // SPOTIFY_H
