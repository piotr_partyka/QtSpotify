import QtQuick 2.10
import QtQuick.Controls 2.3

Page {
    id: player
    width: 800
    height: 480

    title: qsTr("Player")

    Image {
        id: background
        x: 0
        y: 0
        smooth: true
        antialiasing: true
        z: 0
        source: "background.png"
    }

    SwipeView {
        id: coverflow
        width: 550
        height: 367
        anchors.verticalCenterOffset: -43
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        orientation: Qt.Horizontal
        currentIndex: spotify.trackNumber

        Repeater {
            model: spotify.playlistSize
            Loader {
                //active: coverflow.currentIndex == index || coverflow.currentIndex == index+1 || coverflow.currentIndex == index-1
                active: SwipeView.isCurrentItem || SwipeView.isNextItem || SwipeView.isPreviousItem
                sourceComponent: Image {
                    id: cover
                    height: 350
                    width: 350
                    source: spotify.coverList[index]
                    fillMode: Image.PreserveAspectFit

                    SequentialAnimation {
                        id: coverClickedAnim
                        ScaleAnimator{
                            target: cover
                            from: 1
                            to: 0.95
                            duration: 100
                            easing.type: Easing.Linear
                        }

                        ScaleAnimator{
                            target: cover
                            from: 0.95
                            to: 1
                            duration: 100
                            easing.type: Easing.Linear
                        }
                    }

                    MouseArea {
                        id: coverAnimation
                        anchors.fill: cover
                        onClicked: {
                            coverClickedAnim.start()
                            if(spotify.playbackState == false){
                                if(spotify.playbackStatus == false){
                                    spotify.playClicked();
                                }
                                else {
                                    spotify.resumeClicked();
                                }

                                spotify.playbackStatus == true;
                            }
                            else {
                                spotify.playbackStatus == false;
                                spotify.pauseClicked();
                            }
                        }
                    }
                }
            }
        }

        onCurrentIndexChanged: {
            console.log("INDEX CHANGED")
            if(spotify.changedWithTrackEnd == false){
                console.log("SWIPE")
                if(spotify.trackNumber < currentIndex){
                    console.log("NEXT");
                    spotify.nextClicked();
                    spotify.changedWithSwipe = true;
                    spotify.trackNumber++;
                }
                if(spotify.trackNumber > currentIndex){
                    console.log("PREVIOUS");
                    spotify.previousClicked();
                    spotify.changedWithSwipe = true;
                    spotify.trackNumber--;
                }
            }
            else{
                console.log("TRACK ENDED")
            }
        }

        Connections {
            target: spotify
            onTrackEnded: {
                spotify.changedWithTrackEnd = true;
                spotify.getTrackNumber();
                while(coverflow.currentIndex != spotify.trackNumber){
                    if(coverflow.currentIndex > spotify.trackNumber){
                        coverflow.decrementCurrentIndex();
                    }
                    else {
                        coverflow.incrementCurrentIndex();
                    }
                }
                spotify.changedWithTrackEnd = false;
            }
        }
    }

    Slider {
        id: slider
        x: 176
        y: 443
        width: 447
        height: 20
        from: 0
        to: spotify.trackLength
        value: spotify.trackPosition
        onPressedChanged: if(!pressed) spotify.trackSeek(value)
        background: Rectangle {
            x: slider.leftPadding
            y: slider.topPadding + slider.availableHeight / 2 - height / 2
            implicitWidth: 200
            implicitHeight: 8
            width: slider.availableWidth
            height: implicitHeight
            radius: 10
            color: "#bdbebf"

            Rectangle {
                width: slider.visualPosition * parent.width
                height: parent.height
                color: "#21be2b"
                radius: 10
            }
        }

        handle: Rectangle {
            x: slider.leftPadding + slider.visualPosition * (slider.availableWidth - width)
            y: slider.topPadding + slider.availableHeight / 2 - height / 2
            implicitWidth: 25
            implicitHeight: 25
            radius: 13
            color: slider.pressed ? "#f0f0f0" : "#f6f6f6"
            border.color: "#bdbebf"
        }
    }

    Text {
        id: trackPosition
        x: 130
        y: 444
        color: "#afafaf"
        text: spotify.trackPositionLabel
        font.pixelSize: 15
    }

    Text {
        id: trackLength
        x: 632
        y: 444
        color: "#afafaf"
        text: spotify.trackLengthLabel
        font.family: "Tahoma"
        font.pixelSize: 15
    }

    Text {
        id: trackName
        x: 387
        y: 387
        text: spotify.trackName
        anchors.horizontalCenterOffset: 0
        font.bold: true
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 18
        color: "#ffffff"
    }

    Text {
        id: trackArtist
        x: 388
        y: 413
        text: spotify.artist
        verticalAlignment: Text.AlignVCenter
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 15
        color: "#afafaf"
        font.family: "Tahoma"
    }


    Image {
        x: 696
        y: 413
        width: 75
        height: 75
        fillMode: Image.PreserveAspectFit
        source: "icons/queue.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                stackView.push("queue.qml")
            }
        }
    }
}
