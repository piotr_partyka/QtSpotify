﻿#include "spotify.h"
#include <QDesktopServices>
#include <QtNetworkAuth>
#include <QEventLoop>

Spotify::Spotify()
{
    rpc_client = new jcon::JsonRpcWebSocketClient;
    connect(rpc_client, &jcon::JsonRpcClient::receivedEvent, this, &Spotify::parseEvent);
//    mopidyServerAddr = "192.168.0.143:6680/mopidy/";
//    mopidyServerAddr = "localhost:6680/mopidy/";
    mopidyServerAddr = "192.168.0.220:6680/mopidy/";
    rpc_client->connectToServer(QUrl(QString("ws://") + mopidyServerAddr + QString("ws")));
    playbackState = getPlaybackState();
    changedWithSwipe = false;
    changedWithTrackEnd = false;
    UIready = false;
    getTrackNumber();
    qDebug() << trackNumber;
    getTrackInfo();
    getTrackPosition();
    getVolume();

    auto replyHandler = new QOAuthHttpServerReplyHandler(8080, this);
    spotifyWebApi.setReplyHandler(replyHandler);
    spotifyWebApi.setAuthorizationUrl(QUrl("https://accounts.spotify.com/authorize"));
    spotifyWebApi.setAccessTokenUrl(QUrl("https://accounts.spotify.com/api/token"));
    spotifyWebApi.setClientIdentifier("183c9852128949219e22f42044e160d3");
    spotifyWebApi.setClientIdentifierSharedKey("0a2ad5b394924a8bbeb234661d3a6d78");
    spotifyWebApi.setScope("user-read-private user-top-read playlist-read-private playlist-modify-public playlist-modify-private");

    connect(&spotifyWebApi, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser,
             &QDesktopServices::openUrl);

    connect(&spotifyWebApi, &QOAuth2AuthorizationCodeFlow::statusChanged,
            this, &Spotify::authStatusChanged);

    connect(&spotifyWebApi, &QOAuth2AuthorizationCodeFlow::granted,
            this, &Spotify::granted);

    spotifyWebApi.grant();

    trackMslabelTimer = new QTimer(this);
    //connect(trackMslabelTimer, &QTimer::timeout, this, &Spotify::getTrackPosition);
    trackMslabelTimer->start(1000);
}

void Spotify::granted()
{
    qDebug() << "Signal granted received";

    QString token = spotifyWebApi.token();
    qDebug() << "Token: " << token;
    getTracklist();
    UIready = true;
    emit uiReady();
    qDebug() << "Spotify::granted() ended.";
}

void Spotify::authStatusChanged(QAbstractOAuth::Status status)
{
    QString s;
    if (status == QAbstractOAuth::Status::Granted)
        s = "granted";

    if (status == QAbstractOAuth::Status::TemporaryCredentialsReceived) {
        s = "temp credentials";
        //oauth2.refreshAccessToken();
    }

    qDebug() << "Auth Status changed: " + s +  "\n";
}

void Spotify::parseEvent(const QJsonObject obj){
    //qDebug() << "Event:" << obj;

    if(obj.value("event") == "track_playback_started"){
        qDebug() << "Event: track_playback_started";
        auto data = obj.value("tl_track").toObject().value("track").toObject();
        this->album = data.value("album").toObject().value("name").toString();
        this->albumUrl = data.value("album").toObject().value("uri").toString().remove(0, 14);
        auto artistArray = data.value("artists").toArray();
        this->artist = artistArray.at(0).toObject().value("name").toString();
        for(int i=1; i < artistArray.size(); i++){
            artist.push_back(", " + artistArray.at(i).toObject().value("name").toString());
        }
        this->artistUrl = data.value("artists").toArray().at(0).toObject().value("uri").toString();
        this->trackName = data.value("name").toString();
        QString minutes = QString::number(data.value("length").toInt()/60000);
        QString seconds = QString::number(data.value("length").toInt()/1000 - minutes.toInt()*60);
        if (seconds.size() == 1){
            seconds.push_front("0");
        }
        this->trackLengthLabel = minutes + ":" + seconds;
        this->trackLength = data.value("length").toInt();
        //trackNumber = obj.value("tl_track").toObject().value("tlid").toInt();
//        trackNumber = getTrackNumber();

        if(changedWithSwipe == false){
            emit trackEnded();
            //trackNumber++;
        }
        else {
            changedWithSwipe = false;
        }
        emit trackInfoChanged();
        return;
    }
    else if(obj.value("event") == "playback_state_changed"){
        if (obj.value("new_state").toString() == "playing"){
            this->playbackState = 1;
        }
        else {
            this->playbackState = 0;
        }
        qDebug() << "PlaybackState: " << obj.value("new_state").toString();
        emit playbackStateChanged();
    }
//    else if(obj.value("event") == "tracklist_changed"){
//        tracklist.clear();
//        tracklistIDs.clear();
//        coverList.clear();
//        getTracklist();
//    }
}

bool Spotify::getConnectionStatus(){
    return rpc_client->isConnected();
}

bool Spotify::getPlaybackState(){
    auto result = rpc_client->call("core.playback.get_state", false);
    if(result->isSuccess()){
        QVariant res = result->result();
        //qDebug() << "PlaybackState: " << res.toString();
        if(res.toString() == "playing"){
            return 1;
        }
        else {
            return 0;
        }
        return 0;
    }
    else {
        qDebug() << "[ERROR] Couldn't get playback state.";
        return 0;
    }
}

void Spotify::playClicked(int tlid){
    QNetworkRequest request(QUrl(QString("http://") + mopidyServerAddr + QString("rpc")));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QJsonObject params {
        //{"tl_track", QString("none")},
        {"tlid", tlid}
    };

    QJsonObject json {
        {"method", "core.playback.play"},
        {"jsonrpc", "2.0"},
        {"id", 0},
        {"params", params}
    };
    qDebug() << "Request created: " << json;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    manager->post(request, QJsonDocument(json).toJson());
}

void Spotify::resumeClicked(){
    rpc_client->callAsync("core.playback.resume", false);
}

void Spotify::pauseClicked(){
    rpc_client->callAsync("core.playback.pause", false);
}

void Spotify::nextClicked(){
    rpc_client->callAsync("core.playback.next", false);
    trackPosition = 0;
    trackPositionLabel = "0:00";
    emit trackPositionLabelChanged();
}

void Spotify::previousClicked(){
    rpc_client->callAsync("core.playback.previous", false);
    trackPosition = 0;
    trackPositionLabel = "0:00";
    emit trackPositionLabelChanged();
}

void Spotify::trackSeek(int position){
    rpc_client->callAsync("core.playback.seek", true, position);
    qDebug() << "Seeked: " << position;
}

void Spotify::getTrackPosition(){
    if(playbackState == 1)
    {
        auto result = rpc_client->call("core.playback.get_time_position", false);
        if(result->isSuccess()){
            QVariant res = result->result();
            QString minutes = QString::number(res.toInt()/60000);
            QString seconds = QString::number(res.toInt()/1000 - minutes.toInt()*60);
            if (seconds.size() == 1){
                seconds.push_front("0");
            }

            trackPositionLabel = minutes + ":" + seconds;
            trackPosition = res.toInt();
            emit trackPositionLabelChanged();
        }
        else{
            qDebug() << "[ERROR] Couldn't get current song's position.";
            return;
        }
    }
}

void Spotify::getTrackInfo(){
    auto result = rpc_client->call("core.playback.get_current_track", false);
    if(result->isSuccess()){
        QJsonObject res = result->result().toJsonObject();

        //Get track's album from response
        if(res.contains("album")){
            this->album = res.value("album").toObject().value("name").toString();
            this->albumUrl = res.value("album").toObject().value("uri").toString().remove(0, 14);
            qDebug() << "Album: " << this->album;
            qDebug() << "Album URL: " << this->albumUrl;
        }
        else{
            qDebug() << "No 'album' key";
        }

        //Get track's artist from response
        if(res.contains("artists")){
            auto artistArray = res.value("artists").toArray();
            this->artist = artistArray.at(0).toObject().value("name").toString();
            for(int i=1; i < artistArray.size(); i++){
                artist.push_back(", " + artistArray.at(i).toObject().value("name").toString());
            }
            this->artistUrl = res.value("artists").toArray().at(0).toObject().value("uri").toString();
            qDebug() << "Artist: " << this->artist;
            qDebug() << "Artist URL: " << this->artistUrl;
        }
        else{
            qDebug() << "No 'artists' key";
        }

        //Get track's name from response
        if(res.contains("name")){
            this->trackName = res.value("name").toString();
            qDebug() << "Track: " << this->trackName;
        }
        else{
            qDebug() << "No track name key";
        }

        //Get track's length from response
        if(res.contains("length")){
            QString minutes = QString::number(res.value("length").toInt()/60000);
            QString seconds = QString::number(res.value("length").toInt()/1000 - minutes.toInt()*60);
            if (seconds.size() == 1){
                seconds.push_front("0");
            }
            this->trackLengthLabel = minutes + ":" + seconds;
            this->trackLength = res.value("length").toInt();

            qDebug() << "Track length: " << this->trackLengthLabel;
        }
        else{
            qDebug() << "No track length key";
        }

        emit trackInfoChanged();
    }
    else{
        QString err_str = result->toString();
        qDebug() << "ERROR: " << err_str;
    }
}

QString Spotify::getTrackCover(QString url){
    QUrl request("https://api.spotify.com/v1/albums/" + url);
    QString returnVal;
    QEventLoop loop;
    auto reply = spotifyWebApi.get(request);
    QObject::connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << reply->errorString();
        return "ERROR";
    }
    const auto data = reply->readAll();
    auto document = QJsonDocument::fromJson(data);
    returnVal = document.object().value("images").toArray().at(0).toObject().value("url").toString();
    reply->deleteLater();

    return returnVal;
}

void Spotify::getTrackNumber(){
    QNetworkRequest request(QUrl(QString("http://") + mopidyServerAddr + QString("rpc")));
    QEventLoop loop;
    QJsonObject json {
        {"method", "core.tracklist.index"},
        {"jsonrpc", "2.0"},
        {"id", 0}
    };
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    qDebug() << "Request created: " << json;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    auto result = manager->post(request, QJsonDocument(json).toJson());
    connect(result, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();

    if(!result->error()) {
        auto data = QJsonDocument::fromJson(result->readAll());
        trackNumber = data.object().value("result").toInt();
        qDebug() << "TrackNumber is " << trackNumber;
    } else {
      qDebug() << "ERROR: " << result->errorString();
    }
}

void Spotify::getTracklist(){
    auto result = rpc_client->call("core.tracklist.get_tl_tracks", false);
    if(result->isSuccess()){
        QVariant res = result->result();
        QJsonArray playlist = res.toJsonArray();
//        qDebug() << playlist;
        playlistSize = playlist.size();
        qDebug() << "Playlist size: " << playlistSize;

        for(int i=0; i < playlistSize; i++){
            QString cover = getTrackCover(playlist.at(i).toObject().value("track").toObject().value("album").toObject().value("uri").toString().remove(0, 14));
            QString name = playlist.at(i).toObject().value("track").toObject().value("name").toString();
            QJsonArray artists = playlist.at(i).toObject().value("track").toObject().value("artists").toArray();
//            qDebug() << artists;
            QString artist = artists.at(0).toObject().value("name").toString();
            for(int j=1; j<artists.size(); j++){
                artist.push_back(", " + artists.at(j).toObject().value("name").toString());
            }
            QString album = playlist.at(i).toObject().value("track").toObject().value("album").toObject().value("name").toString();
            int tracklistID = playlist.at(i).toObject().value("tlid").toInt();
            coverList.push_back(cover);
            tracklist.push_back(name);
            tracklistIDs.push_back(tracklistID);
            tracklistArtists.push_back(artist);
            tracklistAlbums.push_back(album);
        }
    }
    else{
        qDebug() << "[ERROR] Couldn't get tracklist - " << result->toString();
        return;
    }
}

void Spotify::getVolume(){
    auto result = rpc_client->call("core.mixer.get_volume", false);
    qDebug() << "Getting track number...";
    if(result->isSuccess()){
        volume = result->result().toInt();
        qDebug() << "Got track number.";
    }
    else {
        QString err_str = result->toString();
        qDebug() << "ERROR: " << err_str;
        return;
    }

}

void Spotify::setVolume(int newVolume){
    rpc_client->callAsync("core.mixer.set_volume", true, newVolume);
}

