import QtQuick 2.10
import QtQuick.Controls 2.3

Page {
    title: "Tracklist"
    width: 800
    height: 480

    header: Rectangle {
        color: "grey"
        width: parent.width
        height: 60

        Row {
            id: row
            anchors.fill: parent
            ToolButton {
                width: 70
                height: 70
                Image {
                    width: 20
                    height: 20
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.horizontalCenterOffset: -2
                    anchors.verticalCenter: parent.verticalCenter
                    source: "icons/backArrow.png"
                }
                anchors.left: parent.left
                anchors.leftMargin: -5
                anchors.verticalCenter: parent.verticalCenter
                onClicked: stackView.pop();
            }
            Label {
                text: "Queue"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: 14
            }
        }
    }

    Component {
        id: queueDelegate
        Rectangle {
            id: wrapper
            height: 80
            width: 800
            color: "#404247"

            Text {
                id: name
                width: 400
                elide: Text.ElideRight
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: 20
                text: spotify.tracklist[index]
                anchors.verticalCenterOffset: -9
                anchors.left: parent.left
                color: spotify.trackNumber == index ? "#21be2b" : "#efefef"
                font.pixelSize: 22
                verticalAlignment: Text.AlignVCenter
            }

            Text {
                id: artist
                anchors.verticalCenter: parent.verticalCenter
                text: spotify.tracklistArtists[index]
                anchors.verticalCenterOffset: 12
                anchors.left: parent.left
                anchors.leftMargin: 20
                color: spotify.trackNumber == index ? "#21be2b" : "#efefef"
                font.pixelSize: 17
                verticalAlignment: Text.AlignVCenter
            }

            Text {
                id: album
                width: 350
                elide: Text.ElideRight
                anchors.verticalCenter: parent.verticalCenter
                text: spotify.tracklistAlbums[index]
                anchors.left: parent.left
                anchors.leftMargin: 450
                color: spotify.trackNumber == index ? "#21be2b" : "#efefef"
                font.pixelSize: 18
                verticalAlignment: Text.AlignVCenter
            }

            SequentialAnimation {
                id: wrapperClickedAnim
                ScaleAnimator {
                    target: wrapper
                    from: 1
                    to: 0.95
                    duration: 100
                    easing.type: Easing.Linear
                }

                ScaleAnimator {
                    target: wrapper
                    from: 0.95
                    to: 1
                    duration: 100
                    easing.type: Easing.Linear
                }
            }

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    console.log("Play track number " + index);
                    console.log("TracklistIDs[]: " + spotify.tracklistIDs);
                    wrapperClickedAnim.start();
                    spotify.playClicked(spotify.tracklistIDs[index]);
                }
            }
        }
    }

    ListView {
        id: tracklist
        width: parent.width
        height: parent.height
        x: 0
        y: 0
        spacing: 1
        currentIndex: spotify.trackNumber

        model: spotify.playlistSize
        delegate: queueDelegate
    }
}
