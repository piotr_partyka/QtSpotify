import QtQuick 2.10
import QtQuick.Controls 2.3
import io.qt.Spotify 1.0

ApplicationWindow {
    id: window
    visible: true
    width: 800
    height: 480
    title: qsTr("spotify-client")

    Spotify {
        id: spotify
    }

    Drawer {
        id: drawer
        width: window.width * 0.33
        height: window.height
        dragMargin: 5

        Column {
            id: drawerColumn
            anchors.fill: parent
            spacing: 5
            property int iconLeftMargin: 15
            property int textLeftMargin: 60

            ItemDelegate {
                Image {
                    width: 30
                    height: 30
                    antialiasing: true
                    anchors.left: parent.left
                    anchors.leftMargin: drawerColumn.iconLeftMargin
                    anchors.verticalCenter: parent.verticalCenter
                    source: "icons/play.png"
                }
                Text {
                    y: 0
                    color: "#f0eeee"
                    text: "Now playing"
                    anchors.left: parent.left
                    anchors.leftMargin: drawerColumn.textLeftMargin
                    anchors.verticalCenter: parent.verticalCenter
                    font.pointSize: 15
                }
                width: parent.width
                onClicked: {
                    stackView.push("player.qml")
                    drawer.close()
                }
            }

            ItemDelegate {
                Image {
                    width: 30
                    height: 30
                    antialiasing: true
                    anchors.left: parent.left
                    anchors.leftMargin: drawerColumn.iconLeftMargin
                    anchors.verticalCenter: parent.verticalCenter
                    source: "icons/spotify.png"
                }
                Text {
                    y: 0
                    color: "#f0eeee"
                    text: "Browse"
                    anchors.left: parent.left
                    anchors.leftMargin: drawerColumn.textLeftMargin
                    anchors.verticalCenter: parent.verticalCenter
                    font.pointSize: 15
                }
                width: parent.width
                onClicked: {
                    stackView.push("browse.qml")
                    drawer.close()
                }
            }

            ItemDelegate {
                Image {
                    width: 30
                    height: 30
                    antialiasing: true
                    anchors.left: parent.left
                    anchors.leftMargin: drawerColumn.iconLeftMargin
                    anchors.verticalCenter: parent.verticalCenter
                    source: "icons/playlists.png"
                }
                Text {
                    y: 0
                    color: "#f0eeee"
                    text: "Playlists"
                    anchors.left: parent.left
                    anchors.leftMargin: drawerColumn.textLeftMargin
                    anchors.verticalCenter: parent.verticalCenter
                    font.pointSize: 15
                }
                width: parent.width
                onClicked: {
                    stackView.push("playlists.qml")
                    drawer.close()
                }
            }

            ItemDelegate {
                Image {
                    width: 30
                    height: 30
                    antialiasing: true
                    anchors.left: parent.left
                    anchors.leftMargin: drawerColumn.iconLeftMargin
                    anchors.verticalCenter: parent.verticalCenter
                    source: "icons/songs.png"
                }
                Text {
                    y: 0
                    color: "#f0eeee"
                    text: "Songs"
                    anchors.left: parent.left
                    anchors.leftMargin: drawerColumn.textLeftMargin
                    anchors.verticalCenter: parent.verticalCenter
                    font.pointSize: 15
                }
                width: parent.width
                onClicked: {
                    stackView.push("songs.qml")
                    drawer.close()
                }
            }

            ItemDelegate {
                Image {
                    width: 30
                    height: 30
                    antialiasing: true
                    anchors.left: parent.left
                    anchors.leftMargin: drawerColumn.iconLeftMargin
                    anchors.verticalCenter: parent.verticalCenter
                    source: "icons/albums.png"
                }
                Text {
                    y: 0
                    color: "#f0eeee"
                    text: "Albums"
                    anchors.left: parent.left
                    anchors.leftMargin: drawerColumn.textLeftMargin
                    anchors.verticalCenter: parent.verticalCenter
                    font.pointSize: 15
                }
                width: parent.width
                onClicked: {
                    stackView.push("albums.qml")
                    drawer.close()
                }
            }

            ItemDelegate {
                Image {
                    width: 30
                    height: 30
                    antialiasing: true
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: drawerColumn.iconLeftMargin
                    source: "icons/artists.png"
                }
                Text {
                    y: 0
                    color: "#f0eeee"
                    text: "Artists"
                    anchors.left: parent.left
                    anchors.leftMargin: drawerColumn.textLeftMargin
                    anchors.verticalCenter: parent.verticalCenter
                    font.pointSize: 15
                }
                width: parent.width
                onClicked: {
                    stackView.push("artists.qml")
                    drawer.close()
                }
            }
        }

        Image {
            id: searchIcon
            x: 0
            width: 30
            height: 30
            y: 380
            antialiasing: true
            anchors.left: parent.left
            anchors.leftMargin: drawerColumn.iconLeftMargin
            source: "icons/search.png"
        }

        TextField {
            y: 386
            width: drawer.width - anchors.left
            anchors.left: drawerColumn.left
            anchors.leftMargin: drawerColumn.textLeftMargin
            font.pixelSize: 15
            color: "#f0eeee"
            text: "Search"
            anchors.verticalCenter: searchIcon.verticalCenter
        }

        Image {
            id: speaker
            y: 430
            width: 30
            height: 30
            z: 1
            anchors.left: parent.left
            anchors.leftMargin: drawerColumn.iconLeftMargin
            source: "icons/speaker.png"
        }

        Slider {
            id: volume
            width: 180
            height: 20
            anchors.leftMargin: 54
            anchors.verticalCenter: speaker.verticalCenter
            anchors.left: drawerColumn.left
            from: 0
            to: 100
            value: spotify.volume
            onPressedChanged: if(!pressed) spotify.setVolume(value)
            background: Rectangle {
                x: volume.leftPadding
                y: volume.topPadding + volume.availableHeight / 2 - height / 2
                width: volume.availableWidth
                height: implicitHeight
                color: "#bdbebf"
                radius: 10
                implicitWidth: 200
                implicitHeight: 6
                Rectangle {
                    width: volume.visualPosition * parent.width
                    height: parent.height
                    color: "#21be2b"
                    radius: 10
                }
            }

            handle: Rectangle {
                x: volume.leftPadding + volume.visualPosition * (volume.availableWidth - width)
                y: volume.topPadding + volume.availableHeight / 2 - height / 2
                implicitWidth: 20
                implicitHeight: 20
                radius: 13
                color: volume.pressed ? "#f0f0f0" : "#f6f6f6"
                border.color: "#bdbebf"
            }
        }

    }

    StackView {
        id: stackView
        initialItem: "loadingScreen.qml"
        anchors.fill: parent

        Connections {
            target: spotify

            onUiReady: {
                console.log("Loading screen poped.")
                stackView.push("player.qml");
            }
        }
    }

}
