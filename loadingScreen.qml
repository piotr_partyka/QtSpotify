import QtQuick 2.0
import QtQuick.Controls 2.3

Page {
    id:loadingScreen
    width: 800
    height: 480

    BusyIndicator {
        id: busyIndicator
        x: 370
        y: 210
        z: 1

        running: true
    }
}
